package com.example.bankwebapp;

import com.example.bankwebapp.model.Account;
import com.example.bankwebapp.model.Transaction;
import com.example.bankwebapp.services.AccountServices;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AccountServicesImplTest {
    AccountServices accountServices;

    @Test
    void viewAccount() throws Exception {
        Account account = new Account();
        account.setAccountName("Ngbede Alike");
        account.setAccountNumber("1234567890");
        account.setInitialDeposit(500.00);
        account.setAccountPassword("1234");
        List<Account> accountList = new ArrayList<>();
        accountList.add(account);
        assertEquals(true, accountList.contains(account));
    }

    @Test
    void viewTransactions() {
        Transaction transaction = new Transaction();
        transaction.setNarration("foody");
        transaction.setAccountBalanceAfterTransaction(300.00);
        transaction.setAmount(300.00);
        transaction.setAccountNumber("12345678");

        List<Transaction> transactionList = new ArrayList<>();
       transactionList.add(transaction);
       assertEquals(true, transactionList.contains(transaction));
    }

    @Test
    void createAccount() {
        Account account = new Account();
        account.setAccountName("Ngbede Alike");
        account.setAccountNumber("1234567890");
        account.setInitialDeposit(500.00);
        account.setAccountPassword("1234");
        List<Account> accountList = new ArrayList<>();
        accountList.add(account);
        assertEquals(500, account.getInitialDeposit());
    }

    @Test
    void withdrawal() {

    }

    @Test
    void deposit() {
    }
}