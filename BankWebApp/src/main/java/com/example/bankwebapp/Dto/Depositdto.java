package com.example.bankwebapp.Dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Depositdto {
    String accountNumber;
    Double amount;
    String narration;
}
