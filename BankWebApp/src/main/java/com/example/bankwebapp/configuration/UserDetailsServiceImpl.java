package com.example.bankwebapp.configuration;

;
import com.example.bankwebapp.model.Account;
import com.example.bankwebapp.AccountServicesImpl;
import com.example.bankwebapp.services.AccountServices;
import lombok.SneakyThrows;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.security.auth.login.AccountNotFoundException;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {

        User.UserBuilder userBuilder = null;
        AccountServices service = new AccountServicesImpl();
        for (Account account : service.viewAccount()){
            if (account.getAccountName().equalsIgnoreCase(accountName)){
                userBuilder =  User.withUsername(accountName);
                userBuilder.password(new BCryptPasswordEncoder().encode(account.getAccountPassword()));
            }else {
                throw new AccountNotFoundException("account not found");
            }
        }

        return userBuilder.build();
    }
}
