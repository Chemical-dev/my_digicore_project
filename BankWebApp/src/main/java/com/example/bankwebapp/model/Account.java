package com.example.bankwebapp.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Data
public class Account {
    String accountName;
    Double initialDeposit;
    Double balance;
    String accountNumber ;
    String accountPassword;


}
