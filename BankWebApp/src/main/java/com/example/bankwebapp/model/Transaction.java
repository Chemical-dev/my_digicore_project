package com.example.bankwebapp.model;

import com.example.bankwebapp.enums.TransactionType;
import lombok.*;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor

@Setter
@Getter

public class Transaction {
    Date transactionDate;
    TransactionType transactionType;
    String narration;
    Double amount;
    Double accountBalanceAfterTransaction;
    String accountNumber;
}
