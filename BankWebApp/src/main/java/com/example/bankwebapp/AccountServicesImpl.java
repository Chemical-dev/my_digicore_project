package com.example.bankwebapp;

import com.example.bankwebapp.enums.TransactionType;
import com.example.bankwebapp.model.Account;
import com.example.bankwebapp.model.Transaction;
import com.example.bankwebapp.payload.ApiResponse;
import com.example.bankwebapp.services.AccountRegistrationDto;
import com.example.bankwebapp.services.AccountServices;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import java.util.*;

@Service
public class AccountServicesImpl implements AccountServices {
    Map<String, Account> accountVault = new HashMap<>();
    List<Transaction> transactionsVault = new ArrayList<>();

    @Override
    public Account viewAccount (String accountNumber) throws Exception {

        Account account = accountVault.get(accountNumber);
        if (account.getAccountNumber().equals(accountNumber)) {

            return accountVault.get(accountNumber);

        }
        throw new Exception("Insufficient Balance");
    }



    @Override
    public List<Transaction> viewTransactions(String accountNumber) {
        List<Transaction> transactions = new ArrayList<>();
        for(Transaction t: transactionsVault) {
          if(t.getAccountNumber().equals(accountNumber)) {
              transactions.add(t);
          }
        }
        return transactions;
    }

    @Override
    public ApiResponse createAccount(AccountRegistrationDto accountRegistrationDto) throws Exception {
        Random rand = new Random();
        ApiResponse response = null;
        Account account = new Account();
        Transaction transaction = new Transaction();
        for (Map.Entry<String, Account> map : accountVault.entrySet()) {
            if (map.getValue().getAccountName().equals(accountRegistrationDto.getAccountName())) {
                return null;
            }
        }
//        if(accountVault.get(account.getAccountName()).equals(accountRegistrationDto.getAccountName())) {
//            throw new Exception("account already exists");
//        }
        account.setAccountName(accountRegistrationDto.getAccountName());
        account.setBalance(accountRegistrationDto.getInitialDeposit());
        account.setAccountPassword(accountRegistrationDto.getAccountPassword());
        account.setBalance(accountRegistrationDto.getInitialDeposit());

        account.setAccountNumber(String.valueOf(rand.nextInt(1000000 * 10000)));

        transaction.setTransactionDate(new Date());
        transaction.setTransactionType(TransactionType.Deposit);
        transaction.setAccountBalanceAfterTransaction(accountRegistrationDto.getInitialDeposit());
        transaction.setNarration(accountRegistrationDto.getDepositNarration());
        transaction.setAmount(accountRegistrationDto.getInitialDeposit());
        transaction.setAccountNumber(account.getAccountNumber());

        accountVault.put(account.getAccountNumber(), account);
        transactionsVault.add(transaction);
        response = new ApiResponse(Boolean.TRUE, "Account successfully created", HttpStatus.OK);
        return response;
    }

    @Override
    public Account withdrawal(String accountNumber, Double withdrawnAmount, String accountPassword, String narration) throws Exception {
        Account account = accountVault.get(accountNumber);
        Transaction transaction = new Transaction();
        Double existingBalance = account.getBalance();
        if (withdrawnAmount < existingBalance) {
            Double newBalance = existingBalance - withdrawnAmount;
            account.setBalance(newBalance);
            accountVault.replace(accountNumber, account);

            transaction.setAmount(withdrawnAmount);
            transaction.setTransactionType(TransactionType.Withdrawal);
            transaction.setTransactionDate(new Date());
            transaction.setAccountBalanceAfterTransaction(newBalance);
            transaction.setAccountNumber(accountNumber);
            transaction.setNarration(narration);
            transactionsVault.add(transaction);
            //
            // accountVault.get(accountNumber).setBalance(accountVault.get(accountNumber).getBalance()-withdrawnAmount);
            return account;
        } else {
            return null;
        }

    }

    @Override
    public Account deposit(String accountNumber, Double amount, String narration) throws Exception {
        Account account = accountVault.get(accountNumber);
        Transaction transaction = new Transaction();
        Double existingBalance = account.getBalance();


            if(amount > 1000000 || amount < 1) {
                return null;
            }
                Double newBalance = existingBalance + amount;
                account.setBalance(newBalance);
                accountVault.replace(accountNumber, account);

                transaction.setAccountNumber(accountNumber);
                transaction.setTransactionType(TransactionType.Deposit);
                transaction.setTransactionDate(new Date());
                transaction.setAccountBalanceAfterTransaction(newBalance);
                transaction.setAmount(amount);
                transaction.setNarration(narration);
                transactionsVault.add(transaction);


        return account;
    }

    @Override
    public List<Account> viewAccount() throws Exception {
        return null;
    }
}
