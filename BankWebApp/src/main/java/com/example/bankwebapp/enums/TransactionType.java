package com.example.bankwebapp.enums;

public enum TransactionType {
    Deposit, Withdrawal
}
