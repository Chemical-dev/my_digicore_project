package com.example.bankwebapp.controller;


import com.example.bankwebapp.AccountServicesImpl;
import com.example.bankwebapp.Dto.JwtRequest;
import com.example.bankwebapp.Dto.JwtResponse;
import com.example.bankwebapp.configuration.JwtTokenUtil;
import com.example.bankwebapp.configuration.UserDetailsServiceImpl;
import com.example.bankwebapp.payload.ApiResponse;
import com.example.bankwebapp.services.AccountRegistrationDto;
import com.example.bankwebapp.services.AccountServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    AccountServices service = new AccountServicesImpl();
@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
                    (jwtRequest.getAccountName(), jwtRequest.getPassword()));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getAccountName());

        final String jwtToken = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(jwtToken));
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> registerAccount(@RequestBody AccountRegistrationDto account) throws Exception {
        ApiResponse response = service.createAccount(account);
        HttpStatus status = response.getSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(response, status);
    }
}
