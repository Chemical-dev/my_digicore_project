package com.example.bankwebapp.controller;


import com.example.bankwebapp.model.Account;
import com.example.bankwebapp.Dto.Depositdto;
import com.example.bankwebapp.Dto.Withdrawaldto;
import com.example.bankwebapp.payload.ApiResponse;
import com.example.bankwebapp.services.AccountRegistrationDto;
import com.example.bankwebapp.services.AccountServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/account")
public class AccountController {
  @Autowired
  AccountServices accountServices;
  HttpHeaders headers = new HttpHeaders();
  String message= "success";

  @PostMapping("/createAccount")
  @ResponseBody
  public ResponseEntity<?> registerUser(@RequestBody AccountRegistrationDto accountRegistrationDto) throws Exception {
         ApiResponse response = accountServices.createAccount(accountRegistrationDto);

         String message= "success";
    if(response!=null){
         headers.add("message",message);
      return new ResponseEntity<>(response,headers,HttpStatus.OK);
    }
         headers.add("message","Failed");
    return new ResponseEntity<>("Account already exists",headers,HttpStatus.BAD_REQUEST);

  }

  @GetMapping("/viewAccount/{accountNumber}")
  public ResponseEntity<?> viewAccount(@PathVariable(value = "accountNumber") String accountNumber) throws Exception {
         Account account = accountServices.viewAccount(accountNumber);
         if(account!=null){
                  headers.add("message",message);
          return new ResponseEntity<>(account,headers,HttpStatus.OK);
         }

          headers.add("message","Failed");
          return new ResponseEntity<>("Account not available",headers,HttpStatus.BAD_REQUEST);
  }

  @PostMapping("/withdrawal")
  public ResponseEntity<?> withdrawal(@RequestBody Withdrawaldto withdrawaldto) throws Exception {
      Account account = accountServices.withdrawal(withdrawaldto.getAccountNumber(), withdrawaldto.getWithdrawnAmount(),withdrawaldto.getAccountPassword(), withdrawaldto.getNarration());

      if(account!=null){
          headers.add("message",message);
          return new ResponseEntity<>(account,headers,HttpStatus.OK);

      }
      headers.add("message","Failed");
      return new ResponseEntity<>("Insufficient balance",headers,HttpStatus.BAD_REQUEST);

  }


  @GetMapping("/viewTransactions/{accountNumber}")
  public ResponseEntity<?> viewTransactios(@PathVariable(value = "accountNumber") String accountNumber){
    return ResponseEntity.ok(accountServices.viewTransactions(accountNumber));
  }

  @ResponseBody
  @PostMapping("/deposit")
  public ResponseEntity<?> deposit(@RequestBody Depositdto depositdto) throws Exception {
          Account account = accountServices.deposit(depositdto.getAccountNumber(), depositdto.getAmount(), depositdto.getNarration());
          String message= "success";
          if(account!=null){
          headers.add("message",message);
          return new ResponseEntity<>(account,headers,HttpStatus.OK);

  }
           headers.add("message","Failed");
          return new ResponseEntity<>("Invalid amount",headers,HttpStatus.BAD_REQUEST);
  }
}


