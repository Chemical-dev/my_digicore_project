package com.example.bankwebapp.services;

import com.example.bankwebapp.model.Account;
import com.example.bankwebapp.model.Transaction;
import com.example.bankwebapp.payload.ApiResponse;

import java.util.List;

public interface AccountServices {
    Account viewAccount(String accountNumber) throws Exception;

    List<Transaction> viewTransactions(String accountNumber);

    ApiResponse createAccount(AccountRegistrationDto accountRegistrationDto) throws Exception;
    Account withdrawal(String accountNumber, Double withdrawnAmount, String accountPassword, String narration) throws Exception;
    Account deposit(String accountNumber, Double amount, String narration) throws Exception;
    List<Account> viewAccount() throws Exception;

}


